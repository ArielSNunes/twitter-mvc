<div class="container mt-5 pt-3">
    <h3 class="text-center">Cadastro</h3>
    <div class="row no-gutters">
        <?php if (!empty($aviso)) : ?>
        <div class="col-12 offset-0 col-lg-8 offset-lg-2 py-3">
                <div class="alert alert-danger text-center" role="alert">
                    <?= $aviso; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="row no-gutters">
        <div class="col-12 offset-0 col-lg-8 offset-lg-2">
            <form method="POST">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <br>
                    <input type="text" name="nome" id="nome" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <br>
                    <input type="email" name="email" id="email" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="senha">Senha</label>
                    <br>
                    <input type="password" name="senha" id="senha" class="form-control" />
                </div>
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 px-0 py-1 py-md-0 pr-md-1">
                        <a href="<?= BASE_URL ?>/login" class="btn btn-primary btn-block">Já Possui Conta?</a>
                    </div>
                    <div class="col-12 col-md-6 px-0 py-1 py-md-0 pl-md-1">
                        <input type="submit" value="Cadastrar" class="btn btn-success btn-block bt-md-lg" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>