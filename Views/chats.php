<div class="container pt-5 chats">
    <div class="accordion" id="accordionExample">
        <?php foreach ($chats as $chatId) : ?>
            <div class="card border rounded mx-2">
                <div class="card-header text-center" id="heading<?= $chatId ?>">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $chatId ?>" aria-expanded="true" aria-controls="collapse<?= $chatId ?>">
                            Chat com <?= $nomes[$chatId] ?>
                        </button>
                    </h5>
                </div>
                <div id="collapse<?= $chatId ?>" class="collapse show" aria-labelledby="heading<?= $chatId ?>" data-parent="#accordionExample">
                    <div class="card-body">
                        <?php foreach ($conversas[$chatId] as $chat) : ?>
                        <?php if ($chat['id_usuario'] === $_SESSION['twlg']) : ?>
                            <div class="d-flex justify-content-end">
                                <div class="alert alert-success">
                        <?php else : ?>
                            <div class="d-flex justify-content-start">
                                <div class="alert alert-primary">
                        <?php endif; ?>
                                    <small>
                                        <?= $chat['data_mensagem']; ?>
                                    </small>
                                    <br>
                                    <?= $chat['mensagem']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <form method="POST" action="<?= BASE_URL ?>/chats/newmessage/<?= $chatId; ?>">
                            <div class="form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="message"></textarea>
                            </div>
                            <input type="submit" value="Enviar" class="btn btn-success btn-block" />
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>