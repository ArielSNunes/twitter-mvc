<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Twitter</title>
        <link rel="stylesheet" href="<?php echo (BASE_URL) ?>/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo (BASE_URL) ?>/assets/css/styles.css"/>
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-primary">
            <a class="navbar-brand" href="<?php echo (BASE_URL) ?>">Twitter</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo (BASE_URL) ?>">Home</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <?php if (!empty($_SESSION['twlg']) && !empty($viewData['nome'])) : ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <?= $viewData['nome']; ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= BASE_URL ?>/chats">Chats</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo (BASE_URL) ?>/login/sair">Sair</a> 
                        </li>
                    <?php else : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo (BASE_URL) ?>/login/cadastro">Cadastro</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo (BASE_URL) ?>/login">Login</a> 
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
            <?php $this->loadView($viewName, $viewData); ?>
        
        <script src="<?php echo (BASE_URL) ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo (BASE_URL) ?>/assets/js/popper.min.js"></script>
        <script src="<?php echo (BASE_URL) ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo (BASE_URL) ?>/assets/js/app.js"></script>
    </body>

</html>