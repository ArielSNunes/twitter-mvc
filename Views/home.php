<div class="container pt-5 pt-sm-5 home">
    <div class="row justify-content-center">
        <div class="col-10 col-lg-8 order-2 order-lg-1 py-3 py-md-0">
            <div class="row no-gutters">
                <div class="col-12">
                    <form method="POST">
                        <label for="message" class="text-center">Mensagem: </label>
                        <br>
                        <textarea name="message" id="message" cols="0" rows="20" class="textarea"></textarea>
                        <br><br>
                        <input type="submit" value="Enviar" class="btn btn-success btn-block"/>
                    </form>
                </div>
                <div class="col-12 pt-3">
                    <?php foreach ($feed as $post) : ?>
                        <div class="border border-success rounded p-3 text-center m-1" role="alert">
                            <h6 class="p-0 m-0"><?= $post['nome']; ?>:</h6>
                            <p class="p-0 m-0"><?= $post['mensagem']; ?></p>
                            <h6 class="pt-1 m-0"><?= date('d/m/Y (H:i)', strtotime($post['data_post'])); ?></h6>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="col-10 col-lg-4 border border-primary rounded py-5 text-center order-1 order-lg-2">
            <h5>Relacionamentos</h5>
            <div class="row no-gutters">
                <div class="col-6">
                    <p class="btn btn-success m-0">Seguindo <?= $qtSeguidos; ?></p> 
                </div>
                <div class="col-6">
                    <p class="btn btn-primary m-0"><?= $qtSeguidores; ?> Seguidores</p>
                </div>
            </div>
            <?php if(count($sugestoes) >0): ?>
                <div class="row no-gutters pt-4 d-none d-md-block">
                    <div class="col">
                        <h5>Sugestões de Amigos</h5>
                        <?php foreach ($sugestoes as $sugestao) : ?>
                            <div class="row no-gutters justify-content-between">
                                <div class="col-12 col-md-6">
                                    <?= $sugestao['nome'] ?>
                                </div>
                                <?php if ($sugestao['seguido'] === '0') : ?>
                                    <div class="col-12 col-md-6">
                                        <a href="<?= BASE_URL ?>/home/follow/<?= $sugestao['id'] ?>">Seguir</a>
                                    </div>
                                <?php else : ?>
                                    <div class="col-12 col-md-6">
                                        <a href="<?= BASE_URL ?>/home/unfollow/<?= $sugestao['id'] ?>">Deixar de Seguir</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ;?>
            <?php if (count($amigos) > 0): ?>
                <div class="row no-gutters pt-4 d-none d-md-block">
                    <div class="col">
                        <h5>Últimos Amigos</h5>
                        <?php foreach ($amigos as $amigo) : ?>
                            <div class="row no-gutters justify-content-between">
                                <div class="col-12 col-md-6">
                                    <?= $amigo['nome'] ?>
                                </div>
                                <div class="col-12 col-md-6">
                                    <a href="<?= BASE_URL ?>/home/unfollow/<?= $amigo['id'] ?>">Deixar de Seguir</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ;?>
            <?php if (count($seguidos) > 0): ?>
                <div class="row no-gutters pt-4 d-none d-md-block">
                    <div class="col">
                        <h5>Meus Seguidores</h5>
                        <?php foreach ($seguidos as $seguido) : ?>
                            <div class="row no-gutters justify-content-between text-center">
                                <div class="col-12">
                                    <?= $seguido['nome'] ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ;?>
        </div>
    </div>
</div>