<?php 
class Relacionamentos extends Model
{
    private $userId;
    public function __construct($id = null)
    {
        parent::__construct();
        if (empty($id))
            return;
        $this->userId = $id;
    }
    public function alteraSeguir($id, $operation)
    {
        switch ($operation) {
            case 'follow':
                if ($this->createRelacionamento($id))
                    return true;
                return false;
                break;
            case 'unfollow':
                if ($this->deleteRelacionamento($id))
                    return true;
                return false;
                break;
            default:
                return false;
                break;
        }
    }
    private function deleteRelacionamento($id)
    {
        if (!$this->relacionamentoExists($id))
            return false;
        $sql = "DELETE FROM relacionamentos WHERE id_seguidor = :idSeguidor AND id_seguido = :idSeguido";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('idSeguidor', $this->userId);
        $sql->bindValue('idSeguido', $id);
        $sql->execute();
        return true;
    }
    private function createRelacionamento($id)
    {
        if ($this->relacionamentoExists($id))
            return false;
        $sql = "INSERT INTO relacionamentos (id_seguidor, id_seguido, data_add) VALUES (:idSeguidor,:idSeguido, NOW())";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('idSeguidor', $this->userId);
        $sql->bindValue('idSeguido', $id);
        $sql->execute();
        return true;
    }
    private function relacionamentoExists($idSeguido)
    {
        $sql = "SELECT * FROM relacionamentos WHERE id_seguidor = :id AND id_seguido = :idSeguido";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->bindValue('idSeguido', $idSeguido);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return true;
        return false;
    }
}