<?php
class Posts extends Model
{
    private $userId;
    public function __construct($id = null)
    {
        parent::__construct();
        if (empty($id))
            return;
        $this->userId = $id;
    }
    public function criarPost($msg)
    {
        $sql = "INSERT INTO posts (id_usuario, data_post, mensagem) VALUES (:id, NOW() , :msg)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->bindValue('msg', $msg);
        $sql->execute();
        return true;
    }
    public function exibirFeed($listaDeUsuarios, $limit)
    {
        $listaDeUsuarios = implode(',', $listaDeUsuarios);
        $sql = "SELECT id, data_post, mensagem, (
            SELECT nome from usuarios WHERE usuarios.id = posts.id_usuario
        ) as nome FROM posts WHERE id_usuario IN (" . $listaDeUsuarios . ") ORDER BY data_post desc LIMIT :limit ";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('limit', $limit, PDO::PARAM_INT);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
}