<?php
class Chats extends Model
{
    private $userId;
    public function __construct($id = null)
    {
        parent::__construct();
        if (empty($id))
            return;
        $this->userId = $id;
    }
    public function getChats()
    {
        $sql = "
        SELECT DISTINCT `id_chat`
        FROM `chats_mensagens`
        LEFT JOIN `chats`
        ON `chats`.`id` = `chats_mensagens`.`id_chat`
        WHERE `chats`.`id_criador` = :id OR `chats`.`id_chamado` = :id;";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function getConversasByChatId($id)
    {
        $sql = "
        SELECT 
            `chats_mensagens`.`id`, 
            `chats_mensagens`.`mensagem`, 
            `chats_mensagens`.`data_mensagem`, 
            `usuarios`.`nome`, 
            `chats_mensagens`.`id_usuario`,
            `chats`.`id` as chat_id
        FROM `chats_mensagens`
        LEFT JOIN `chats`
        ON `chats`.`id` = `chats_mensagens`.`id_chat`
        LEFT JOIN `usuarios`
        ON `chats_mensagens`.`id_usuario` = `usuarios`.`id` 
        WHERE `chats`.`id` = :chatId";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('chatId', $id);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function addChat($chatId, $msg)
    {
        $sql = "INSERT into chats_mensagens (id_chat, id_usuario, data_mensagem, mensagem) VALUES (:chatId, :userId, NOW(), :msg)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('chatId', $chatId);
        $sql->bindValue('userId', $this->userId);
        $sql->bindValue('msg', $msg);
        $sql->execute();
    }
}