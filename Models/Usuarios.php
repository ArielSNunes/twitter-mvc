<?php
class Usuarios extends Model
{
    private $userId;
    public function __construct($id = null)
    {
        parent::__construct();
        if (empty($id))
            return;
        $this->userId = $id;
    }
    public function estaLogado()
    {
        if (isset($_SESSION['twlg']) && !empty($_SESSION['twlg']))
            return true;
        return false;
    }
    public function emailExiste($email)
    {
        $sql = "SELECT * FROM usuarios WHERE email = :email";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('email', $email);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return true;
        return false;
    }
    public function criarUsuario($nome, $email, $senha)
    {
        $sql = "INSERT into usuarios (nome, email, senha) VALUES (:nome, :email, :senha)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('nome', $nome);
        $sql->bindValue('email', $email);
        $sql->bindValue('senha', $senha);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $this->db->lastInsertId();
        return false;
    }
    public function fazerLogin($email, $senha)
    {
        $sql = "SELECT * FROM usuarios WHERE email = :email AND senha = :senha";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('email', $email);
        $sql->bindValue('senha', $senha);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetch()['id'];
        return false;
    }
    public function getNome()
    {
        if (empty($this->userId))
            return false;
        $sql = "SELECT nome FROM usuarios WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetch()['nome'];
        return false;
    }
    public function getUsuariosAleatorios($limite)
    {
        if (empty($limite))
            return false;
        $sql = "
        SELECT usuarios.id, nome, (
            SELECT COUNT(*) from `relacionamentos` WHERE `relacionamentos`.`id_seguidor` = :id AND `relacionamentos`.`id_seguido` = usuarios.`id`
            ) AS seguido 
        FROM usuarios
        WHERE NOT usuarios.id = :id
        HAVING seguido = 0
        ORDER BY RAND()
        LIMIT :limit";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('limit', $limite, PDO::PARAM_INT);
        $sql->bindValue('id', $this->userId);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function getNumeroSeguidos()
    {
        $sql = "SELECT * FROM relacionamentos WHERE id_seguidor = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function getNumeroSeguidores()
    {
        $sql = "SELECT * FROM relacionamentos WHERE id_seguido = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function getSeguidores()
    {
        $sql = "SELECT * FROM relacionamentos WHERE id_seguidor = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function getMeusAmigos($limite)
    {
        $sql = "SELECT `usuarios`.`id`, `usuarios`.`nome`
        FROM `usuarios`
        RIGHT JOIN `relacionamentos`
        ON `relacionamentos`.`id_seguido` = `usuarios`.`id`
        WHERE relacionamentos.`id_seguidor` = :id
        ORDER BY `relacionamentos`.`data_add` desc
        LIMIT :limit";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->bindValue('limit', $limite, PDO::PARAM_INT);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
    public function getListaDeSeguidos($limite)
    {
        $sql = "SELECT `usuarios`.`id`, `usuarios`.`nome`
        FROM `usuarios`
        RIGHT JOIN `relacionamentos`
        ON `relacionamentos`.`id_seguidor` = `usuarios`.`id`
        WHERE relacionamentos.`id_seguido` = :id
        ORDER BY `relacionamentos`.`data_add` desc
        LIMIT :limit;";
        $sql = $this->db->prepare($sql);
        $sql->bindValue('id', $this->userId);
        $sql->bindValue('limit', $limite, PDO::PARAM_INT);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return [];
    }
}