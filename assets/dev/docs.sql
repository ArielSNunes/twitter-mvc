-- Arquivo para guardar os SQL que forem desenvolvidos, não é realmente usado no MVC
create table usuarios (
    id int unsigned not null auto_increment,
    nome varchar(100) not null,
    email varchar(100) not null UNIQUE,
    senha varchar(32) not null,
    primary key (id)
) engine=InnoDB;

create table posts (
    id int unsigned not null auto_increment,
    id_usuario int unsigned not null,
    data_post datetime default CURRENT_TIMESTAMP,
    mensagem text not null,
    primary key (id)
) engine=InnoDB;

alter table posts add constraint fk_id_usuario foreign key (id_usuario) references usuarios(id);

create table relacionamentos (
    id int unsigned not null auto_increment,
    id_seguidor int unsigned not null,
    id_seguido int unsigned not null,
    primary key (id)
) engine=InnoDB;

alter table relacionamentos add constraint fk_id_seguidor foreign key (id_seguidor) references usuarios(id);
alter table relacionamentos add constraint fk_id_seguido foreign key (id_seguido) references usuarios(id);
alter table relacionamentos add data_add datetime default CURRENT_TIMESTAMP;

create table chat (
    id int unsigned not null auto_increment,
    id_criador int unsigned not null,
    id_chamado int unsigned not null,
    primary key (id)
) engine=InnoDB;

create table chat_mensagem (
    id int unsigned not null auto_increment,
    id_chat int unsigned not null,
    id_usuario int unsigned not null,
    data_mensagem datetime default CURRENT_TIMESTAMP,
    mensagem text not null,
    primary key (id)
) engine=InnoDB;

alter table chat add constraint fk_id_criador foreign key(id_criador) references usuarios(id);
alter table chat add constraint fk_id_chamado foreign key(id_chamado) references usuarios(id);
alter table chat_mensagem add constraint fk_id_chat foreign key(id_chat) references chat(id);
alter table chat_mensagem add constraint fk_chat_id_usuario foreign key(id_usuario) references usuarios(id);