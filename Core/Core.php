<?php
class Core
{
    public function run()
    {
        $url = '/';
        if (isset($_GET['url']))
            $url .= addslashes($_GET['url']);

        $currentController = 'HomeController';
        $currentAction = 'index';
        $params = [];

        if (!empty($url) && $url !== '/') {
            $url = explode('/', $url);
            array_shift($url);
            $currentController = ucfirst($url[0]) . 'Controller';
            array_shift($url);
            if (isset($url[0]) && !empty($url[0]))
                $currentAction = $url[0];
            array_shift($url);
            if (count($url) > 0)
                $params = $url;
        }

        if (!file_exists('Controllers' . DIRECTORY_SEPARATOR . $currentController . '.php') ||
            !method_exists($currentController, $currentAction)) {
            $currentController = 'NotFoundController';
            $currentAction = 'index';
        }

        $c = new $currentController();
        call_user_func_array([$c, $currentAction], $params);
    }
}