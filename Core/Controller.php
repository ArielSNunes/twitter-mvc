<?php
class Controller
{
    public function __construct()
    {
    }
    public function loadView($viewName, $viewData = [])
    {
        extract($viewData);
        require('Views' . DIRECTORY_SEPARATOR . $viewName . '.php');
    }
    public function loadTemplate($viewName, $viewData = [])
    {
        require('Views' . DIRECTORY_SEPARATOR . 'template.php');
    }
}