<?php
session_start();
require('vendor/autoload.php');
require('config.php');
spl_autoload_register(function ($class) {
    $foldersInApp = [
        'Controllers',
        'Models',
        'Core'
    ];
    foreach ($foldersInApp as $folder) {
        if (file_exists($folder . DIRECTORY_SEPARATOR . $class . '.php'))
            require($folder . DIRECTORY_SEPARATOR . $class . '.php');
    }
});

$core = new Core();
$core->run();