<?php

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $usuarios = new Usuarios();
        if (!$usuarios->estaLogado())
            header("Location: " . BASE_URL . '/login');
    }
    public function index()
    {
        $dados = [
            'nome' => null,
            'qtSeguidos' => 0,
            'qtSeguidores' => 0,
            'sugestoes' => [],
            'amigos' => [],
            'seguidos' => []
        ];
        $posts = new Posts($_SESSION['twlg']);
        $usuarios = new Usuarios($_SESSION['twlg']);
        $relacionamentos = new Relacionamentos($_SESSION['twlg']);
        if (isset($_POST['message']))
            $msg = addslashes($_POST['message']);
        if (!empty($msg))
            $posts->criarPost($msg);

        $dados['nome'] = $usuarios->getNome();
        $dados['qtSeguidos'] = count($usuarios->getNumeroSeguidos());
        $dados['qtSeguidores'] = count($usuarios->getNumeroSeguidores());
        $dados['sugestoes'] = $usuarios->getUsuariosAleatorios(5);
        $dados['amigos'] = $usuarios->getMeusAmigos(5);
        $dados['seguidos'] = $usuarios->getListaDeSeguidos(5);
        $listaDeSeguidos = [];
        foreach ($usuarios->getNumeroSeguidos() as $seguido) {
            $listaDeSeguidos[] = $seguido['id_seguido'];
        }
        $listaDeSeguidos[] = $_SESSION['twlg'];
        $dados['feed'] = $posts->exibirFeed($listaDeSeguidos, 10);
        $this->loadTemplate('home', $dados);
    }
    public function follow($id)
    {
        if (empty($id))
            return;
        $rel = new Relacionamentos($_SESSION['twlg']);
        $id = addslashes($id);
        $rel->alteraSeguir($id, 'follow');
        header("Location: " . BASE_URL);
    }
    public function unfollow($id)
    {
        if (empty($id))
            return;
        $rel = new Relacionamentos($_SESSION['twlg']);
        $id = addslashes($id);
        $rel->alteraSeguir($id, 'unfollow');
        header("Location: " . BASE_URL);
    }
}