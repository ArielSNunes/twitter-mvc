<?php

class LoginController extends Controller
{
    public function index()
    {
        if (isset($_SESSION['twlg']) && !empty($_SESSION['twlg']))
            header("Location: " . BASE_URL);
        $dados = [];
        $dados['aviso'] = null;
        if (isset($_POST['email']) && isset($_POST['senha'])) {
            $usuarios = new Usuarios();
            $email = addslashes($_POST['email']);
            $senha = md5($_POST['senha']);
            if (empty($email) || empty($senha))
                $dados['aviso'] = 'Preencha todos os campos';
            if (!empty($dados['aviso']))
                return $this->loadTemplate('cadastro', $dados);
            $_SESSION['twlg'] = $usuarios->fazerLogin($email, $senha);
            header("Location: " . BASE_URL);
        }
        return $this->loadTemplate('login');
    }
    public function cadastro()
    {
        if (isset($_SESSION['twlg']) && !empty($_SESSION['twlg']))
            header("Location: " . BASE_URL);
        $dados = [];
        $dados['aviso'] = null;
        if (isset($_POST['nome']) && isset($_POST['email']) && isset($_POST['senha'])) {
            $usuarios = new Usuarios();
            $nome = addslashes($_POST['nome']);
            $email = addslashes($_POST['email']);
            $senha = md5($_POST['senha']);
            if (empty($nome) || empty($email) || empty($senha))
                $dados['aviso'] = 'Preencha todos os campos';
            if ($usuarios->emailExiste($email))
                $dados['aviso'] = 'Email Já Cadastrado';
            if (!empty($dados['aviso']))
                return $this->loadTemplate('cadastro', $dados);
            $id = $usuarios->criarUsuario($nome, $email, $senha);
            if (isset($id) && !empty($id))
                $_SESSION['twlg'] = $id;
            header("Location: " . BASE_URL);
        }
        return $this->loadTemplate('cadastro', $dados);
    }
    public function sair()
    {
        unset($_SESSION['twlg']);
        header("Location: " . BASE_URL);
    }
}