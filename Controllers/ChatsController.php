<?php
class ChatsController extends Controller
{
    public function __construct()
    {
        $usuarios = new Usuarios();
        if (!$usuarios->estaLogado())
            header("Location: " . BASE_URL . '/login');
    }
    public function index()
    {
        $dados = [];
        $usuarios = new Usuarios($_SESSION['twlg']);
        $chats = new Chats($_SESSION['twlg']);
        $dados['nome'] = $usuarios->getNome();
        $chatsIds = array_map(function ($chat) {
            return $chat['id_chat'];
        }, $chats->getChats());
        $dados['chats'] = $chatsIds;
        $dados['conversas'] = [];
        $dados['nomes'] = [];
        foreach ($dados['chats'] as $chatId) {
            $dados['conversas'][$chatId] = $chats->getConversasByChatId($chatId);
        }
        foreach ($dados['conversas'] as $chat) {
            foreach ($chat as $msg) {
                if ($msg['id_usuario'] !== $_SESSION['twlg'])
                    $dados['nomes'][$msg['chat_id']] = $msg['nome'];
            }
        }
        $this->loadTemplate('chats', $dados);
    }
    public function newmessage($chatId)
    {
        $chatId = addslashes($chatId);
        if (isset($_POST['message']))
            $message = addslashes($_POST['message']);
        $chats = new Chats($_SESSION['twlg']);
        $chats->addChat($chatId, $message);
        header("Location: " . BASE_URL . '/chats');
    }
}